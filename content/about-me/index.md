---
title: 'About me'
---

An experienced technical writer.

- My GitLab profile: [rdickenson](https://gitlab.com/rdickenson)
- My GitLab team member profile: [Russell Dickenson](https://about.gitlab.com/company/team/#rdickenson)
- My job description: [Senior Technical Writer](https://handbook.gitlab.com/job-families/product/technical-writer/#senior-technical-writer)
